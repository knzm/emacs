@c This is part of the Emacs manual.
@c Copyright (C) 1985-1987, 1993-1995, 1997, 2000-2011
@c   Free Software Foundation, Inc.
@c See file emacs.texi for copying conditions.
@node Windows, Frames, Buffers, Top
@chapter Multiple Windows
@cindex windows in Emacs
@cindex multiple windows in Emacs

  Emacs can split a frame into two or many windows.  Multiple windows
can display parts of different buffers, or different parts of one
buffer.  Multiple frames always imply multiple windows, because each
frame has its own set of windows.  Each window belongs to one and only
one frame.

@menu
* Basic Window::        Introduction to Emacs windows.
* Split Window::        New windows are made by splitting existing windows.
* Other Window::        Moving to another window or doing something to it.
* Pop Up Window::       Finding a file or buffer in another window.
* Change Window::       Deleting windows and changing their sizes.
* Displaying Buffers::  How Emacs picks a window for displaying a buffer.
* Window Convenience::  Convenience functions for window handling.
@end menu

@node Basic Window
@section Concepts of Emacs Windows

  Each Emacs window displays one Emacs buffer at any time.  A single
buffer may appear in more than one window; if it does, any changes in
its text are displayed in all the windows where it appears.  But these
windows can show different parts of the buffer, because each window
has its own value of point.

@cindex selected window
  At any time, one Emacs window is the @dfn{selected window}; the
buffer this window is displaying is the current buffer.  The terminal's
cursor shows the location of point in this window.  Each other window
has a location of point as well.  On text-only terminals, there is no
way to show where those locations are, since the terminal has only one
cursor.  On a graphical display, the location of point in a
non-selected window is indicated by a hollow box; the cursor in the
selected window is blinking or solid.

  Commands to move point affect the value of point for the selected Emacs
window only.  They do not change the value of point in other Emacs
windows, even those showing the same buffer.  The same is true for commands
such as @kbd{C-x b} to switch buffers in the selected window;
they do not affect other windows at all.  However, there are other commands
such as @kbd{C-x 4 b} that select a different window and switch buffers in
it.  Also, all commands that display information in a window, including
(for example) @kbd{C-h f} (@code{describe-function}) and @kbd{C-x C-b}
(@code{list-buffers}), work by switching buffers in a nonselected window
without affecting the selected window.

  When multiple windows show the same buffer, they can have different
regions, because they can have different values of point.  However,
they all have the same value for the mark, because each buffer has
only one mark position.

  Each window has its own mode line, which displays the buffer name,
modification status and major and minor modes of the buffer that is
displayed in the window.  The selected window's mode line appears in a
different color.  @xref{Mode Line}, for full details on the mode line.

@node Split Window
@section Splitting Windows

@table @kbd
@item C-x 2
Split the selected window into two windows, one above the other
(@code{split-window-vertically}).
@item C-x 3
Split the selected window into two windows positioned side by side
(@code{split-window-horizontally}).
@item C-Mouse-2
In the mode line or scroll bar of a window, split that window.
@end table

@kindex C-x 2
@findex split-window-vertically
  The command @kbd{C-x 2} (@code{split-window-vertically}) breaks the
selected window into two windows, one above the other.  Both windows start
out displaying the same buffer, with the same value of point.  By default
the two windows each get half the height of the window that was split; a
numeric argument specifies how many lines to give to the top window.

@kindex C-x 3
@findex split-window-horizontally
  @kbd{C-x 3} (@code{split-window-horizontally}) breaks the selected
window into two side-by-side windows.  A numeric argument specifies how
many columns to give the one on the left.  If you are not using
scrollbars, a vertical line separates the two windows.
You can customize its color with the face @code{vertical-border}.
Windows that are not the full width of the screen have mode lines, but
they are truncated.  On terminals where Emacs does not support
highlighting, truncated mode lines sometimes do not appear in inverse
video.

@kindex C-Mouse-2 @r{(scroll bar)}
  You can split a window horizontally or vertically by clicking
@kbd{C-Mouse-2} in the mode line or the scroll bar.  The line of
splitting goes through the place where you click: if you click on the
mode line, the new scroll bar goes above the spot; if you click in the
scroll bar, the mode line of the split window is side by side with
your click.

@vindex truncate-partial-width-windows
  When a window occupies less than the full width of the frame, it may
become too narrow for most of the text lines in its buffer.  If most of
its lines are continued (@pxref{Continuation Lines}), the buffer may
become difficult to read.  Therefore, Emacs automatically truncates
lines if the window width becomes narrower than 50 columns.  This
truncation occurs regardless of the value of the variable
@code{truncate-lines} (@pxref{Line Truncation}); it is instead
controlled by the variable @code{truncate-partial-width-windows}.  If
the value of @code{truncate-partial-width-windows} is a positive integer
(the default is 50), that specifies the minimum width for a
partial-width window before automatic line truncation occurs; if the
value is @code{nil}, automatic line truncation is disabled; and for any
other non-@code{nil} value, Emacs truncates lines in every partial-width
window regardless of its width.

  Horizontal scrolling is often used in side-by-side windows.
@xref{Horizontal Scrolling}.

@vindex split-window-keep-point
  If @code{split-window-keep-point} is non-@code{nil}, the default,
both of the windows resulting from @kbd{C-x 2} inherit the value of
point from the window that was split.  This means that scrolling is
inevitable.  If this variable is @code{nil}, then @kbd{C-x 2} tries to
avoid scrolling the text currently visible on the screen, by putting
point in each window at a position already visible in the window.  It
also selects whichever window contains the screen line that the cursor
was previously on.  Some users prefer that mode on slow terminals.

@node Other Window
@section Using Other Windows

@table @kbd
@item C-x o
Select another window (@code{other-window}).  That is @kbd{o}, not zero.
@item C-M-v
Scroll the next window (@code{scroll-other-window}).
@item M-x compare-windows
Find next place where the text in the selected window does not match
the text in the next window.
@item Mouse-1
@kbd{Mouse-1}, in a window's mode line, selects that window
but does not move point in it (@code{mouse-select-window}).
@end table

@kindex C-x o
@findex other-window
  To select a different window, click with @kbd{Mouse-1} on its mode
line.  With the keyboard, you can switch windows by typing @kbd{C-x o}
(@code{other-window}).  That is an @kbd{o}, for ``other,'' not a zero.
When there are more than two windows, this command moves through all the
windows in a cyclic order, generally top to bottom and left to right.
After the rightmost and bottommost window, it goes back to the one at
the upper left corner.  A numeric argument means to move several steps
in the cyclic order of windows.  A negative argument moves around the
cycle in the opposite order.  When the minibuffer is active, the
minibuffer is the last window in the cycle; you can switch from the
minibuffer window to one of the other windows, and later switch back and
finish supplying the minibuffer argument that is requested.
@xref{Minibuffer Edit}.

@kindex C-M-v
@findex scroll-other-window
  The usual scrolling commands (@pxref{Display}) apply to the selected
window only, but there is one command to scroll the next window.
@kbd{C-M-v} (@code{scroll-other-window}) scrolls the window that
@kbd{C-x o} would select.  It takes arguments, positive and negative,
like @kbd{C-v}.  (In the minibuffer, @kbd{C-M-v} scrolls the window
that contains the minibuffer help display, if any, rather than the
next window in the standard cyclic order.)

  The command @kbd{M-x compare-windows} lets you compare two files or
buffers visible in two windows, by moving through them to the next
mismatch.  @xref{Comparing Files}, for details.

@vindex mouse-autoselect-window
  If you set @code{mouse-autoselect-window} to a non-@code{nil} value,
moving the mouse into a different window selects that window.  This
feature is off by default.

@node Pop Up Window
@section Displaying in Another Window

@cindex selecting buffers in other windows
@kindex C-x 4
  @kbd{C-x 4} is a prefix key for a variety of commands that switch to
a buffer in a different window---either another existing window, or a
new window created by splitting the selected window.  @xref{Window
Choice}, for how Emacs picks or creates the window to use.

@table @kbd
@findex switch-to-buffer-other-window
@item C-x 4 b @var{bufname} @key{RET}
Select buffer @var{bufname} in another window
(@code{switch-to-buffer-other-window}).

@findex display-buffer
@item C-x 4 C-o @var{bufname} @key{RET}
Display buffer @var{bufname} in some window, without trying to select
it (@code{display-buffer}).  @xref{Displaying Buffers}, for details
about how the window is chosen.

@findex find-file-other-window
@item C-x 4 f @var{filename} @key{RET}
Visit file @var{filename} and select its buffer in another window
(@code{find-file-other-window}).  @xref{Visiting}.

@findex dired-other-window
@item C-x 4 d @var{directory} @key{RET}
Select a Dired buffer for directory @var{directory} in another window
(@code{dired-other-window}).  @xref{Dired}.

@findex mail-other-window
@item C-x 4 m
Start composing a mail message, similar to @kbd{C-x m} (@pxref{Sending
Mail}), but in another window (@code{mail-other-window}).

@findex find-tag-other-window
@item C-x 4 .
Find a tag in the current tags table, similar to @kbd{M-.}
(@pxref{Tags}), but in another window (@code{find-tag-other-window}).
@item C-x 4 r @var{filename} @key{RET}
Visit file @var{filename} read-only, and select its buffer in another
window (@code{find-file-read-only-other-window}).  @xref{Visiting}.
@end table

@node Change Window
@section Deleting and Rearranging Windows

@table @kbd
@item C-x 0
Delete the selected window (@code{delete-window}).  The last character
in this key sequence is a zero.
@item C-x 1
Delete all windows in the selected frame except the selected window
(@code{delete-other-windows}).
@item C-x 4 0
Delete the selected window and kill the buffer that was showing in it
(@code{kill-buffer-and-window}).  The last character in this key
sequence is a zero.
@item C-x ^
Make selected window taller (@code{enlarge-window}).
@item C-x @}
Make selected window wider (@code{enlarge-window-horizontally}).
@item C-x @{
Make selected window narrower (@code{shrink-window-horizontally}).
@item C-x -
Shrink this window if its buffer doesn't need so many lines
(@code{shrink-window-if-larger-than-buffer}).
@item C-x +
Make all windows the same height (@code{balance-windows}).
@end table

@kindex C-x 0
@findex delete-window
  To delete a window, type @kbd{C-x 0} (@code{delete-window}).  (That is
a zero.)  The space occupied by the deleted window is given to an
adjacent window (but not the minibuffer window, even if that is active
at the time).  Once a window is deleted, its attributes are forgotten;
only restoring a window configuration can bring it back.  Deleting the
window has no effect on the buffer it used to display; the buffer
continues to exist, and you can select it in any window with @kbd{C-x
b}.

@findex kill-buffer-and-window
@kindex C-x 4 0
  @kbd{C-x 4 0} (@code{kill-buffer-and-window}) is a stronger command
than @kbd{C-x 0}; it kills the current buffer and then deletes the
selected window.

@kindex C-x 1
@findex delete-other-windows
  @kbd{C-x 1} (@code{delete-other-windows}) is more powerful in a
different way; it deletes all the windows except the selected one (and
the minibuffer); the selected window expands to use the whole frame
except for the echo area.

@kindex C-x ^
@findex enlarge-window
@kindex C-x @}
@findex enlarge-window-horizontally
@vindex window-min-height
@vindex window-min-width
  To readjust the division of space among vertically adjacent windows,
use @kbd{C-x ^} (@code{enlarge-window}).  It makes the currently
selected window one line bigger, or as many lines as is specified
with a numeric argument.  With a negative argument, it makes the
selected window smaller.  @kbd{C-x @}}
(@code{enlarge-window-horizontally}) makes the selected window wider by
the specified number of columns.  @kbd{C-x @{}
(@code{shrink-window-horizontally}) makes the selected window narrower
by the specified number of columns.

  When you make a window bigger, the space comes from its peers.  If
this makes any window too small, it is deleted and its space is given
to an adjacent window.  The minimum size is specified by the variables
@code{window-min-height} and @code{window-min-width}.

@kindex C-x -
@findex shrink-window-if-larger-than-buffer
  @kbd{C-x -} (@code{shrink-window-if-larger-than-buffer}) reduces the
height of the selected window, if it is taller than necessary to show
the whole text of the buffer it is displaying.  It gives the extra
lines to other windows in the frame.

@kindex C-x +
@findex balance-windows
  You can also use @kbd{C-x +} (@code{balance-windows}) to even out the
heights of all the windows in the selected frame.

  Mouse clicks on the mode line provide another way to change window
heights and to delete windows.  @xref{Mode Line Mouse}.

@node Displaying Buffers
@section Displaying a Buffer in a Window

  It is a common Emacs operation to display or ``pop up'' some buffer
in response to a user command.  There are several different ways by
which commands do this.

  Many commands, like @kbd{C-x C-f} (@code{find-file}), display the
buffer by ``taking over'' the selected window, expecting that the
user's attention will be diverted to that buffer.  These commands
usually work by calling @code{switch-to-buffer} internally
(@pxref{Select Buffer}).

@findex display-buffer
  Some commands try to display ``intelligently'', trying not to take
over the selected window, e.g. by splitting the selected window and
displaying the desired buffer in the child window.  Such commands,
which include the various help commands (@pxref{Help}), work by
calling @code{display-buffer} internally.  @xref{Window Choice}, for
details.

  Other commands do the same as @code{display-buffer}, and
additionally select the displaying window so that you can begin
editing its buffer.  The command @kbd{C-x `} (@code{next-error}) is
one example (@pxref{Compilation Mode}).  Such commands work by calling
@code{pop-to-buffer} internally.  @xref{Displaying Buffers,,Displaying
Buffers in Windows, elisp, The Emacs Lisp Reference Manual}.

  Commands with names ending in @code{-other-window} behave like
@code{display-buffer}, except that they never display in the selected
window.  Several of these commands are bound in the @kbd{C-x 4} prefix
key (@pxref{Pop Up Window}).

  Commands with names ending in @code{-other-frame} behave like
@code{display-buffer}, except that they (i) never display in the
selected window and (ii) prefer to create a new frame to display the
desired buffer instead of splitting a window---as though the variable
@code{pop-up-frames} is set to @code{t} (@pxref{Window Choice}).
Several of these commands are bound in the @kbd{C-x 5} prefix key.

@menu
* Window Choice::   How @code{display-buffer} works.
@end menu

@node Window Choice
@subsection How @code{display-buffer} works
@findex display-buffer

The @code{display-buffer} command (as well as commands that call it
internally) chooses a window to display using the following steps:

@itemize
@vindex same-window-buffer-names
@vindex same-window-regexps
@item
First, check if the buffer should be displayed in the selected window
regardless of other considerations.  You can tell Emacs to do this by
adding the desired buffer's name to the list
@code{same-window-buffer-names}, or adding a matching regular
expression to the list @code{same-window-regexps}.  By default, these
variables are @code{nil}, so this step is skipped.

@vindex display-buffer-reuse-frames
@item
Otherwise, if the buffer is already displayed in an existing window,
``reuse'' that window.  Normally, only windows on the selected frame
are considered, but windows on other frames are also reusable if you
change @code{display-buffer-reuse-frames} to @code{t}, or if you
change @code{pop-up-frames} (see below) to @code{t}.

@item
Otherwise, if you specified that the buffer should be displayed in a
special frame by customizing @code{special-display-buffer-names} or
@code{special-display-regexps}, do so.  @xref{Special Buffer Frames}.

@vindex pop-up-frames
@item
Otherwise, optionally create a new frame and display the buffer there.
By default, this step is skipped.  To enable it, change the variable
@code{pop-up-frames} to a non-@code{nil} value.  The special value
@code{graphic-only} means to do this only on graphical displays.

@item
Otherwise, try to create a new window by splitting the selected
window, and display the buffer in that new window.

@vindex split-height-threshold
@vindex split-width-threshold
The split can be either vertical or horizontal, depending on the
variables @code{split-height-threshold} and
@code{split-width-threshold}.  These variables should have integer
values.  If @code{split-height-threshold} is smaller than the selected
window's height, the split puts the new window below.  Otherwise, if
@code{split-width-threshold} is smaller than the window's width, the
split puts the new window on the right.  If neither condition holds,
Emacs tries to split so that the new window is below---but only if the
window was not split before (to avoid excessive splitting).

@item
Otherwise, display the buffer in an existing window on the selected
frame.

@item
If all the above methods fail for whatever reason, create a new frame
and display the buffer there.
@end itemize

@node Window Convenience
@section Window Handling Convenience Features and Customization

@findex winner-mode
@cindex Winner mode
@cindex mode, Winner
@cindex undoing window configuration changes
@cindex window configuration changes, undoing
  @kbd{M-x winner-mode} is a global minor mode that records the
changes in the window configuration (i.e. how the frames are
partitioned into windows), so that you can ``undo'' them.  To undo,
use @kbd{C-c left} (@code{winner-undo}).  If you change your mind
while undoing, you can redo the changes you had undone using @kbd{C-c
right} (@code{M-x winner-redo}).  Another way to enable Winner mode is
by customizing the variable @code{winner-mode}.

@cindex Windmove package
@cindex directional window selection
@findex windmove-right
@findex windmove-default-keybindings
  The Windmove commands move directionally between neighboring windows in
a frame.  @kbd{M-x windmove-right} selects the window immediately to the
right of the currently selected one, and similarly for the ``left,'' ``up,''
and ``down'' counterparts.  @kbd{M-x windmove-default-keybindings} binds
these commands to @kbd{S-right} etc.  (Not all terminals support shifted
arrow keys, however.)

  Follow minor mode (@kbd{M-x follow-mode}) synchronizes several
windows on the same buffer so that they always display adjacent
sections of that buffer.  @xref{Follow Mode}.

@vindex scroll-all-mode
@cindex scrolling windows together
@cindex Scroll-all mode
@cindex mode, Scroll-all
  @kbd{M-x scroll-all-mode} provides commands to scroll all visible
windows together.  You can also turn it on by customizing the variable
@code{scroll-all-mode}.  The commands provided are @kbd{M-x
scroll-all-scroll-down-all}, @kbd{M-x scroll-all-page-down-all} and
their corresponding ``up'' equivalents.  To make this mode useful,
you should bind these commands to appropriate keys.
